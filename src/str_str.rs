use crate::Solution;

impl Solution {
    pub fn str_str(haystack: String, needle: String) -> i32 {
        match haystack.find(needle.as_str()) {
            Some(val) => val as i32,
            None => -1
        }
    }

    pub fn str_str_alg(haystack: String, needle: String) -> i32 {
        let haystack_chars: Vec<char> = haystack.clone().chars().into_iter().collect::<Vec<char>>();
        let needle_chars: Vec<char> = needle.clone().chars().into_iter().collect::<Vec<char>>();

        for (idx, item) in haystack_chars.iter().enumerate() {
            if *item == needle_chars[0]
                && idx + needle_chars.len() <= haystack_chars.len()
                && haystack_chars[idx..(idx + needle_chars.len())] == needle_chars
            {
                return idx as i32;
            }
        }

        -1
    }
}

#[test]
fn substr_idx_test() {
    assert_eq!(Solution::str_str(String::from("sadbutsad"), String::from("sad")), 0);
    assert_eq!(Solution::str_str(String::from("leetcode"), String::from("leeto")), -1);
    assert_eq!(Solution::str_str(String::from("aaa"), String::from("aaaa")), -1);

    assert_eq!(Solution::str_str_alg(String::from("sadbutsad"), String::from("sad")), 0);
    assert_eq!(Solution::str_str_alg(String::from("leetcode"), String::from("leeto")), -1);
    assert_eq!(Solution::str_str_alg(String::from("aaa"), String::from("aaaa")), -1);
}
