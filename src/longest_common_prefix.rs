use crate::Solution;

impl Solution {
    pub fn longest_common_prefix(strs: Vec<String>) -> String {
        let mut res: String = String::from("");

        let chars: Vec<char> = strs[0].clone().chars().into_iter().collect::<Vec<char>>();
        for i in 1..=strs[0].len() {
            let test: String = chars.iter().take(i).collect::<String>();

            for idx in 1..strs.len() {
                if !strs[idx].starts_with(test.as_str()) {
                    return res;
                }
            }

            res = test.clone();
        }

        res
    }
}

#[test]
fn get_dialog_ravers() {
    assert_eq!(Solution::longest_common_prefix(vec![String::from("flower"), String::from("flow"), String::from("flight")]), String::from("fl"));
    assert_eq!(Solution::longest_common_prefix(vec![String::from("dog"), String::from("race"), String::from("car")]), String::from(""));
}
