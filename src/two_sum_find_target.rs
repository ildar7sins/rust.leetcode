use crate::Solution;

impl Solution {
    pub fn two_sum(numbers: Vec<i32>, target: i32) -> Vec<i32> {
        for (i, item) in numbers.iter().enumerate() {
            for (k, k_item) in numbers[i+1..].iter().enumerate() {
                if item + k_item == target {
                    return vec![(i+1) as i32, (i+k+2) as i32];
                }
            }
        }

        panic!("Target doesn't exists...");
    }
}

#[test]
fn reverse_string_test() {
    assert_eq!(Solution::two_sum(vec![2,7,11,15], 9), vec![1,2]);
    assert_eq!(Solution::two_sum(vec![2,3,4], 6), vec![1,3]);
}

