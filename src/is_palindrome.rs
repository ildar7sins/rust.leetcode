use crate::Solution;

impl Solution {
    pub fn is_palindrome(x: i32) -> bool {
        x.to_string().chars().rev().collect::<String>() == x.to_string()
    }
}

#[test]
fn is_palindrome_check() {
    assert_eq!(Solution::is_palindrome(10), false);
    assert_eq!(Solution::is_palindrome(121), true);
    assert_eq!(Solution::is_palindrome(-121), false);
}
