use crate::Solution;

#[derive(Debug, Clone, Copy)]
enum Direction {
    Up,
    Down,
}

impl Solution {
    pub fn find_diagonal_order(mat: Vec<Vec<i32>>) -> Vec<i32> {
        if mat.len() == 1 {
            return mat.clone().first().unwrap().clone();
        }

        let mut mat_length: usize = 0;
        let mut mat_map: Vec<i32> = mat
            .iter()
            .map(|item| {
                mat_length += item.len();
                0
            })
            .collect::<Vec<i32>>();

        let mut res: Vec<i32> = vec![];

        let mut i: usize = 0;
        let mat_len: usize = mat.len() - 1;
        let mut direction: Direction = Direction::Up;

        while res.len() < mat_length {
            if mat_map[i] >= 0 {
                res.push(mat[i][mat_map[i] as usize]);

                mat_map[i] = match mat[i].get((mat_map[i] + 1) as usize) {
                    Some(_val) => mat_map[i] + 1,
                    None => -1
                };
            }

            // On the first layer...
            if i == 0 {
                i = match (direction, mat[i].get(mat_map[i] as usize)) {
                    (Direction::Up, Some(_val)) => {
                        direction = Direction::Down;
                        i
                    }
                    (Direction::Up, None) => {
                        direction = Direction::Down;
                        i + 1
                    }
                    (Direction::Down, _) => i + 1
                };
            }

            // On the left side of matrix or last layer...
            else if mat_map[i] - 1 == 0 || i == mat_len {
                i = match (mat_map.get(i + 1), direction) {
                    (Some(_val), Direction::Down) => {
                        direction = Direction::Up;
                        i + 1
                    }
                    (Some(_val), Direction::Up) => {
                        i - 1
                    }
                    (None, Direction::Down) => {
                        direction = Direction::Up;
                        i
                    }
                    (None, Direction::Up) => {
                        i - 1
                    }
                };
            }

            // Just go to direction...
            else {
                i = match direction {
                    Direction::Up => i - 1,
                    Direction::Down => i + 1
                }
            }
        }

        res
    }
}

#[test]
fn get_dialog_ravers() {
    assert_eq!(Solution::find_diagonal_order(vec![vec![1]]), vec![1]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![1], vec![2]]), vec![1, 2]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![6, 9, 7]]), vec![6, 9, 7]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![1, 2], vec![3, 4]]), vec![1, 2, 3, 4]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![2, 5, 8], vec![4, 0, -1]]), vec![2, 5, 4, 0, 8, -1]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]]), vec![1, 2, 4, 7, 5, 3, 6, 8, 9]);
    assert_eq!(Solution::find_diagonal_order(vec![vec![1, 2, 3, 4], vec![5, 6, 7, 8], vec![9,10,11,12], vec![13,14,15,16]]), vec![1,2,5,9,6,3,4,7,10,13,14,11,8,12,15,16]);
}
