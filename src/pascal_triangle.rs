use crate::Solution;

impl Solution {
    pub fn pascal_triangle(num_rows: i32) -> Vec<Vec<i32>> {
        let mut res: Vec<Vec<i32>> = vec![];

        for item in 1..=num_rows {
            res.push(
                (0..item)
                    .map(|i| {
                        match i {
                            val if val == 0 || val == item - 1 => 1,
                            _ => res[(item - 2) as usize][i as usize] + res[(item - 2) as usize][(i - 1) as usize]
                        }
                    })
                    .collect::<Vec<i32>>()
            );
        }

        res
    }
}

#[test]
fn is_palindrome_check() {
    assert_eq!(Solution::pascal_triangle(1), vec![vec![1]]);
    assert_eq!(Solution::pascal_triangle(2), vec![vec![1], vec![1,1]]);
    assert_eq!(Solution::pascal_triangle(3), vec![vec![1], vec![1,1], vec![1,2,1]]);
    assert_eq!(Solution::pascal_triangle(4), vec![vec![1], vec![1,1], vec![1,2,1], vec![1,3,3,1]]);
    assert_eq!(Solution::pascal_triangle(5), vec![vec![1], vec![1, 1], vec![1, 2, 1], vec![1, 3, 3, 1], vec![1, 4, 6, 4, 1]]);
}
