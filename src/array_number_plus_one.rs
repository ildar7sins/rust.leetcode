use crate::Solution;

impl Solution {
    pub fn plus_one(digits: Vec<i32>) -> Vec<i32> {
        let mut res = digits.clone();

        res.reverse();

        let mut in_mind: i32 = 1;

        res = res
            .iter()
            .map(|item| {
                if in_mind == 1 {
                    if *item == 9_i32 {
                        in_mind = 1;
                        return 0_i32;
                    }

                    in_mind = 0;
                    return *item + 1;
                }

                *item
            })
            .collect::<Vec<i32>>();

        if in_mind == 1 {
            res.push(in_mind);
        }

        res.reverse();

        res
    }
}

#[test]
fn plus_one_to_array_number() {
    assert_eq!(Solution::plus_one(vec![1, 2, 3]), vec![1, 2, 4]);
    assert_eq!(Solution::plus_one(vec![4, 3, 2, 1]), vec![4, 3, 2, 2]);
    assert_eq!(Solution::plus_one(vec![9]), vec![1, 0]);
    assert_eq!(Solution::plus_one(vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 0]), vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 1]);
    assert_eq!(Solution::plus_one(vec![7,2,8,5,0,9,1,2,9,5,3,6,6,7,3,2,8,4,3,7,9,5,7,7,4,7,4,9,4,7,0,1,1,1,7,4,0,0,6]), vec![7,2,8,5,0,9,1,2,9,5,3,6,6,7,3,2,8,4,3,7,9,5,7,7,4,7,4,9,4,7,0,1,1,1,7,4,0,0,7]);
}