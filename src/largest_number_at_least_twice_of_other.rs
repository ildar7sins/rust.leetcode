use crate::Solution;

impl Solution {
    pub fn dominant_index(nums: Vec<i32>) -> i32 {
        let mut numbers = nums.clone();
        numbers.sort();

        let max = *numbers.get(numbers.len() - 1).unwrap_or(&0);

        let check = numbers[..numbers.len() - 1]
            .iter()
            .map(|item| item * 2 > max)
            .collect::<Vec<bool>>()
            .contains(&true);

        if check {
            return -1_i32;
        }

        nums.iter().position(|&num| num == max).unwrap() as i32
    }
}

#[test]
fn largest_found() {
    assert_eq!(Solution::dominant_index(vec![3, 6, 1, 0]), 1);
}

#[test]
fn largest_not_found() {
    assert_eq!(Solution::dominant_index(vec![1, 2, 3, 4]), -1);
}