use crate::Solution;

impl Solution {
    pub fn array_pair_sum(nums: Vec<i32>) -> i32 {
        let mut vec = nums.clone();
        vec.sort();

        vec
            .iter()
            .enumerate()
            .step_by(2)
            .map(|(i, item)| std::cmp::min(item, &vec[i + 1]))
            .sum::<i32>()
    }
}

#[test]
fn reverse_string_test() {
    assert_eq!(Solution::array_pair_sum(vec![1, 4, 3, 2]), 4);
    assert_eq!(Solution::array_pair_sum(vec![6, 2, 6, 5, 1, 2]), 9);
}

