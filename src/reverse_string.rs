use crate::Solution;

impl Solution {
    pub fn reverse_string(s: &mut Vec<char>) {
        let s_len = s.len() - 1;

        if s_len > 2 {
            let center = match s_len % 2 {
                0 => s_len / 2 - 1,
                1 => s_len / 2,
                _ => panic!("Unknown length")
            };

            for idx in 0..=center {
                s.swap(idx, s_len - idx);
            }
        }
    }
}

#[test]
fn reverse_string_test() {
    let mut first_test = vec!['h','e','l','l','o'];
    Solution::reverse_string(&mut first_test);
    assert_eq!(first_test, vec!['o','l','l','e','h']);

    let mut second_test = vec!['H','a','n','n','a','h'];
    Solution::reverse_string(&mut second_test);
    assert_eq!(second_test, vec!['h','a','n','n','a','H']);

    let mut third_test = vec!['A',' ','m','a','n',',',' ','a',' ','p','l','a','n',',',' ','a',' ','c','a','n','a','l',':',' ','P','a','n','a','m','a'];
    Solution::reverse_string(&mut third_test);
    assert_eq!(third_test, vec!['a','m','a','n','a','P',' ',':','l','a','n','a','c',' ','a',' ',',','n','a','l','p',' ','a',' ',',','n','a','m',' ','A']);
}

