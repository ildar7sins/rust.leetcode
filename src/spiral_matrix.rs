use crate::Solution;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Solution {
    pub fn spiral_order(matrix: Vec<Vec<i32>>) -> Vec<i32> {
        let mut res: Vec<i32> = vec![];
        let mut position: (usize, usize) = (0, 0);
        let mut current_direction: Direction = Direction::Right;
        let mut selected_cells: HashMap<(usize, usize), bool> = HashMap::new();
        let matrix_length: usize = matrix.iter().map(Vec::len).sum::<usize>();

        while res.len() < matrix_length {
            let (mut row, mut idx) = position;

            if matrix.len() - 1 >= row && matrix[row].len() - 1 >= idx {
                res.push(matrix[row][idx]);
                selected_cells.insert((row, idx), true);
                println!("Row: {row}, Index: {idx}, Direction: {current_direction:?}, Result: {res:?}, Check: {}", res.len() < matrix_length);

                match current_direction {
                    Direction::Up => {
                        if !selected_cells.contains_key(&(row - 1, idx)) {
                            row = row.checked_sub(1).unwrap_or(0);
                        } else {
                            idx += 1;
                            current_direction = Direction::Right;
                        }
                    }
                    Direction::Right => {
                        if matrix[row].len() - 1 >= idx + 1 && !selected_cells.contains_key(&(row, idx + 1)) {
                            idx += 1;
                        } else {
                            row += 1;
                            current_direction = Direction::Down;
                        }
                    }
                    Direction::Down => {
                        if matrix.len() - 1 >= row + 1 && !selected_cells.contains_key(&(row + 1, idx)) {
                            row += 1;
                        } else {
                            idx = idx.checked_sub(1).unwrap_or(0);
                            current_direction = Direction::Left;
                        }
                    }
                    Direction::Left => {
                        if idx >= 1 && !selected_cells.contains_key(&(row, idx - 1)) {
                            idx = idx.checked_sub(1).unwrap_or(0);
                        } else {
                            row = row.checked_sub(1).unwrap_or(0);
                            current_direction = Direction::Up;
                        }
                    }
                };
            }

            position = (row, idx);
        }

        res
    }
}

#[test]
fn get_spiral_order() {
    assert_eq!(Solution::spiral_order(vec![vec![1]]), vec![1]);
    assert_eq!(Solution::spiral_order(vec![vec![1], vec![2]]), vec![1, 2]);
    assert_eq!(Solution::spiral_order(vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]]), vec![1, 2, 3, 6, 9, 8, 7, 4, 5]);
    assert_eq!(Solution::spiral_order(vec![vec![1,2,3,4], vec![5,6,7,8], vec![9,10,11,12]]), vec![1,2,3,4,8,12,11,10,9,5,6,7]);
    assert_eq!(Solution::spiral_order(vec![vec![1,2,3,4,5,6], vec![7,8,9,10,11,12], vec![13,14,15,16,17,18], vec![19,20,21,22,23,24]]), vec![1,2,3,4,5,6,12,18,24,23,22,21,20,19,13,7,8,9,10,11,17,16,15,14]);
}
