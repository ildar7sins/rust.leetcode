#![allow(unused_variables)]

use crate::Solution;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

struct Matrix {
    matrix: Vec<Vec<i32>>
}

impl Matrix {
    fn spiral_iter(&self) -> MatrixSpiralIterator
    {
        MatrixSpiralIterator::init(self.matrix.clone())
    }
}

struct MatrixSpiralIterator {
    matrix: Vec<Vec<i32>>,
    direction: Direction,
    matrix_length: usize,
    position: (usize, usize),
    selected_cells: HashMap<(usize, usize), bool>
}

impl MatrixSpiralIterator {
    fn init(matrix: Vec<Vec<i32>>) -> Self
    {
        let matrix_length = matrix.iter().map(Vec::len).sum::<usize>();
        MatrixSpiralIterator {
            matrix,
            direction: Direction::Right,
            matrix_length,
            position: (0, 0),
            selected_cells: HashMap::new()
        }
    }

    fn go_right(&mut self) -> ()
    {
        let (row, idx) = self.position;

        if self.matrix[row].len() - 1 >= idx + 1 && !self.selected_cells.contains_key(&(row, idx + 1)) {
            self.position = (row, idx + 1);
        } else {
            self.direction = Direction::Down;
            self.position = (row + 1, idx);
        }
    }

    fn go_down(&mut self) -> ()
    {
        let (row, idx) = self.position;

        if self.matrix.len() - 1 >= row + 1 && !self.selected_cells.contains_key(&(row + 1, idx)) {
            self.position = (row + 1, idx);
        } else {
            self.direction = Direction::Left;
            self.position = (row, idx.checked_sub(1).unwrap_or(0));
        }
    }

    fn go_left(&mut self) -> ()
    {
        let (row, idx) = self.position;

        if idx >= 1 && !self.selected_cells.contains_key(&(row, idx - 1)) {
            self.position = (row, idx.checked_sub(1).unwrap_or(0));
        } else {
            self.direction = Direction::Up;
            self.position = (row.checked_sub(1).unwrap_or(0), idx);
        }

    }

    fn go_up(&mut self) -> ()
    {
        let (row, idx) = self.position;

        if !self.selected_cells.contains_key(&(row - 1, idx)) {
            self.position = (row.checked_sub(1).unwrap_or(0), idx);
        } else {
            self.position = (row, idx + 1);
            self.direction = Direction::Right;
        }
    }
}

impl Iterator for MatrixSpiralIterator {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.selected_cells.len() >= self.matrix_length {
            return None;
        }

        let (row, idx) = self.position;
        let current = self.matrix[row][idx];

        self.selected_cells.insert((row, idx), true);

        match self.direction {
            Direction::Up => self.go_up(),
            Direction::Right => self.go_right(),
            Direction::Down => self.go_down(),
            Direction::Left => self.go_left()
        };

        Some(current)
    }
}

impl Solution {
    pub fn spiral_order_struct(matrix: Vec<Vec<i32>>) -> Vec<i32> {
        (Matrix{ matrix }).spiral_iter().collect::<Vec<i32>>()
    }
}

#[test]
fn get_spiral_order_by_struct() {
    assert_eq!(Solution::spiral_order_struct(vec![vec![1]]), vec![1]);
    assert_eq!(Solution::spiral_order_struct(vec![vec![1], vec![2]]), vec![1, 2]);
    assert_eq!(Solution::spiral_order_struct(vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]]), vec![1, 2, 3, 6, 9, 8, 7, 4, 5]);
    assert_eq!(Solution::spiral_order_struct(vec![vec![1,2,3,4], vec![5,6,7,8], vec![9,10,11,12]]), vec![1,2,3,4,8,12,11,10,9,5,6,7]);
    assert_eq!(Solution::spiral_order_struct(vec![vec![1,2,3,4,5,6], vec![7,8,9,10,11,12], vec![13,14,15,16,17,18], vec![19,20,21,22,23,24]]), vec![1,2,3,4,5,6,12,18,24,23,22,21,20,19,13,7,8,9,10,11,17,16,15,14]);
}
